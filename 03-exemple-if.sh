#! /bin/bash
#@isx24559020 ASIX M01-ISO
#Febrer 2021
#Exemple if
# $ prog edat
#----------------------------------

#el que fa el if es que si hi ha mes d'un argument esta malament
 
if [ $# -ne 1 ]:
       echo "Error:nº arguments incorrecte"
       echo "Usage: $0 edad"
       exit 1
fi

echo $1
exit 0
