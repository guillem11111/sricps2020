#! /bin/bash
# @isx24559020 ASIX-M01
#Febrer 2021
#Exercicis llistes dir
#---------------------------------

ERR_ARG=1
ERR_DIR=1

if [ $# -ne 1 ]
then
	echo "nomes 1 argument"
	echo "usage..."
	exit $ERR_ARG
fi

if [ ! -d $1 ]
then	
	echo "ha de ser un directori"
	echo "usage..."
	exit $ERR_DIR
fi

dir=$1
llista_elements=$(ls $dir)

for fit in $llista_elements
do
	if [ -f $fit ]
	then
		echo "$fit: es un fitxer"
	else
		echo "$fit: es una altre cosa"
	fi
done
exit 0
