#! /bin/bash
# @isx24559020 ASIX-M01
#Febrer 2021
#Ens diu les opcions i els aruments
#---------------------------------

#validar arguments

ERR_ARG=1

if [ $# -le 1 ]
then
	echo "ha de tenir mes de un argument"
	echo "usage opcions[...] arg[...]"
	exit $ERR_ARG
fi

opcions=""
arguments=""

for arg in $* 
do
	case $arg in 
		'-a'|'-b'|'-c'|'-d'|'-e'|'-f'|'-g')
		
			opcions="$opcions $arg";;
		*)
			arguments="$arguments $arg";;
	
	esac
done

echo "Opcions: $opcions"
echo "Arguments: $arguments"
