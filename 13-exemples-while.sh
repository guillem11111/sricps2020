#! /bin/bash
# @isx24559020 ASIX-M01
#Febrer 2021
#Numeros del 1 al 10
#---------------------------------

#mostrar del [1-10]

MAX=10
num=1

while [ $num -le $MAX ]
do
	echo "$num"
	((num++))
done
exit 0


