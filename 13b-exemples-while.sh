#! /bin/bash
# @isx24559020 ASIX-M01
#Febrer 2021
#Descripcio: exemples bucle while
#---------------------------------

#numerar stdin linia a linia i en majuscules

num=1
while read -r line
do
	echo "$num: $line" | tr '[a-z]' '[A-Z]'
	((num++))
done
exit 0


#numera stdin fins a token FI

read -r line
while [ "$line" != "FI" ]
do
	echo"$line"
	read -r line
done
exit 0


#mostrar els arguments

num=1
while [ "$#" -gt 0 ]
do
	echo "$num: $1, $#, $*"
	num=$((num+1))
	shift
done
exit 0


#mostrar els arguments

while [ -n "$1" ]
do
	echo "$1 $#: $*"
	shift
done
exit 0


#compador recreixent del arg renut [n-0]

MIN=0
num=$1

while [ $num -ge $MIN ]
do
	echo -n  "$num"
	((num--))
done
exit 0


#numeri la entrada estandar línia a línia

num=1
while read -r line
do
	echo "$num: $line"
	((num++))
done
exit 0
