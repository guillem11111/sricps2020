#! /bin/bash
# @isx24559020 ASIX-M01
#Febrer 2021 
#
#llistar el directori si existeix
# 
# -------------------------------
#validem arguments
if [ $# -ne 1 ]; then
  echo "ERROR: num args incorrecte"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi

#Validar si es un -h
if [ $1 = "-h"  ]; then
	echo "Help de la ordre 07-directori"
	echo "@ isx24559020 m01 curs 2021"
	exit 0
fi



mydir=$1
# Validar existeix el directori
if ! [ -d $mydir ]; then
  echo "ERROR: $mydir no és un directori"
  echo "usage: $0 dir"
  exit $ERR_NODIR
fi

#xixa

ls $mydir
exit 0
