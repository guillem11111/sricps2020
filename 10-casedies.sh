#! /bin/bash
# @isx24559020 ASIX-M01
#Febrer 2021
#primer script
#---------------------------------

ERR_NARGS=1
ERR_NORANG=2
OK=0

#validem arguments
if [ $# -ne 1 ]; then
	  echo "ERROR: num args incorrecte"
	  echo "usage: $0 mes"
	  exit $ERR_NARGS
fi
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ] 
then
	echo "ERROR: valor argument no valid"
	echo "usage: $0 mes (valors [1,12]"
	exit $ERR_NORANG

fi
exit 0

#calculem quans dies de mes te

case $1 in
	"1"|"3"|"5"|"7"|"8"|"10"|"12")
		echo"te 31 dies de mes";;
	"4"|"6"|"9"|"11")
		echo"te 30 dies de mes";;
	"2")
		echo"te 28 dies de mes";;
esac
exit 0
