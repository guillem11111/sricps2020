#! /bin/bash
# @isx24559020 ASIX-M01
#Febrer 2021
#Exemples case
#---------------------------------

#1) exemple vocals i consonants

case $1 in
	[aeiou])
		echo"es una vocal";;
	[bcdfghjklmnoqrstvwxyz])
		echo"es una consonant";;
	*)
		echo"es una altre cosa";;
esac
exit 0



#2) exemple noms

case $1 in
	"pere"|"pau"|"joan")
		echo " es un noi";;
	"marta"|"anna"|"julia")
		echo"es una noia";;


#3) Dia de la setmana laborable o fesitu

case $1 in
	"dl"|"dt"|"dc"|"dj"|"dv")
		echo"Es un dia laborable";;
	"ds"|"dm")
		echo"Es un dia festiu";;
	*)
		echo"Es una altre cosa";;
esac
exit 0


