#! /bin/bash
# Exemples de funcions
#

function getHome(){
	
	login=$1
	home=$(grep "^$login:" /etc/passwd | cut -d: -f6)
	if [ $? -ne 0 ];
	then
		return 1
	else
		echo $home
	    	return 0	

	fi
}

function getHoleList(){
	ERR_ARG=1
	status=0
	
	if [ $# -lt 1 ];
	then
		echo "Error: Minim un argument"
		echo "Usage: $0 login[...]"
		return $ERR_ARG
	fi

	for login in $*
	do	
		grep "^$login:" /etc/passwd  
		if [ $? -eq 0 ];
		then
			getHome	$login
			
		else
			echo "Error: El login $login no existeix" >> /dev/stderr
			status=2
		fi		
	done
	return $status
}


function getSize(){

	ERR_DIR=1
	ERR_DIR2=2

	homedir=$1
	
	grep ":$homedir:[^:]*$" /etc/passwd &> /dev/null
	if [ $? -ne 0 ];
	then
		echo "Error: El directori home no existeix"
		echo "Usage: $0 dir"
		return $ERR_DIR	
	else
		if [ -d $homedir ];
		then
			du -sb $homedir | cut -f1
			return 0
		else
			return $ERR_DIR
		fi	
	fi
}


function getSizeIn(){

	ERR_LOGIN=3

	while read -r login
	do
		grep "^$login:" /etc/passwd &> /dev/stderr
		if [ $? -eq 0 ];
		then
			home=$( grep "^$login:" /etc/passwd | cut -d: -f6)
			getSize $home

		else
			echo "Error: El login $login no existeix"
			return $ERR_LOGIN
		fi
	done
}


function getAllUsersSize(){
	
	llista_home=$(cut -d: -f6 /etc/passwd)

	for home in $llista_home
	do
		grep ":$home:[^:]*$" /etc/passwd | cut -d: -f1
		getSize $home
	done
}
