#! /bin/bash
# Exemples de funcions
#


function showUser(){
	ERR_ARG=1
	ERR_LOG=2

	if [ $# -lt 1 ];
	then
		echo "Error: Minim un login"
		echo "Usage: $0 login"
		return $ERR_ARG
	fi

	login=$1
	linia=$(grep "^$login:" /etc/passwd)
	if [ $? -ne 0 ];
	then
		echo "Error: El login no existeix"
		echo "Usage: $0 login"
		return $ERR_LOG
	fi

	uid=$( echo $linia | cut -d: -f3)
	gid=$( echo $linia | cut -d: -f4)
	home=$( echo $linia | cut -d: -f6)
	shell=$( echo $linia | cut -d: -f7)

	echo "usuari: $login"
	echo "uid: $uid"
	echo "gid: $gid"
	echo "home: $home"
	echo "shell: $shell"
}

function showGroup(){
        ERR_ARG=1
        ERR_LOG=2

        if [ $# -lt 1 ];
        then
                echo "Error: Minim un login"
                echo "Usage: $0 login"
                return $ERR_ARG
        fi

        login=$1
        linia=$(grep "^$login:" /etc/passwd)
        if [ $? -ne 0 ];
        then
                echo "Error: El login no existeix"
                echo "Usage: $0 login"
                return $ERR_LOG
        fi

        uid=$( echo $linia | cut -d: -f3)
        gid=$( echo $linia | cut -d: -f4)
        gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
        home=$( echo $linia | cut -d: -f6)
        shell=$( echo $linia | cut -d: -f7)

        echo "usuari: $login"
        echo "uid: $uid"
        echo "group: $gname"
        echo "gid: $gid"
        echo "home: $home"
        echo "shell: $shell"
}
        
	
function showUserList(){
	ERR_ARG=1
        status=0

        if [ $# -lt 1 ];
        then
                echo "Error: Minim un login"
                echo "Usage: $0 login"
                return $ERR_ARG
        fi

        for login in $*
	do
        	linia=$(grep "^$login:" /etc/passwd)
        	if [ $? -ne 0 ];
       		then
                	echo "Error: El login no existeix"
                	echo "Usage: $0 login"
                	status=2
       		fi

        	uid=$( echo $linia | cut -d: -f3)
        	gid=$( echo $linia | cut -d: -f4)
        	gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
        	home=$( echo $linia | cut -d: -f6)
        	shell=$( echo $linia | cut -d: -f7)

        	echo "usuari: $login"
        	echo "uid: $uid"
        	echo "group: $gname"
        	echo "gid: $gid"
        	echo "home: $home"
        	echo "shell: $shell"
	
	done
	return $status
}
function showUserIn(){
        status=0

        while read -r login
	do
                linia=$(grep "^$login:" /etc/passwd)
                if [ $? -ne 0 ];
                then
                        echo "Error: El login no existeix"
                        echo "Usage: $0 login"
                        status=2
                fi

                uid=$( echo $linia | cut -d: -f3)
                gid=$( echo $linia | cut -d: -f4)
                gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
                home=$( echo $linia | cut -d: -f6)
                shell=$( echo $linia | cut -d: -f7)

                echo "usuari: $login"
                echo "uid: $uid"
                echo "group: $gname"
                echo "gid: $gid"
                echo "home: $home"
                echo "shell: $shell"

        done
        return $status
}

function showGroupMainMembers(){
  ERR_ARG=1
  ERR_GNAME=2
 
	if [ $# -lt 1 ];
        then
                echo "Error: Minim un argument"
                echo "Usage: $0 gname"
                return $ERR_ARG
        fi
	gname=$1

	gid=$(grep "^$gname:" /etc/group | cut -d: -f3)
	if [ $? -ne 0 ];
        then
        	echo "Error: El login no existeix"
                echo "Usage: $0 login"
                return $ERR_GNAME
        fi
	echo "$gname($gid): "
	grep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd | cut -d: -f1,3,6,7 | sort -k2n -t: | tr '[a-z]' '[A-Z]' | sed 's/:/  /g' 

	

}


function showAllGroupMainMembers(){
        
	llista_gname=$( cut -d: -f1 /etc/group | sort -u)	
	for gname in $llista_gname
	do
		gid=$( grep "^$gname:" /etc/group | cut -d: -f3)
        	echo "$gname($gid): "
        	grep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd | cut -d: -f1,3,6,7 | sort -k1n -t: 
	done
}

function showAllGroupMainMembers2(){

        llista_gname=$( cut -d: -f1 /etc/group | sort -u)       
        for gname in $llista_gname
        do
		gid=$( grep "^$gname:" /etc/group | cut -d: -f3)
		count=$(grep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd | wc -l)
                echo "$gname($gid): $count"
                grep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd | cut -d: -f1,3,6,7 | sort -k1n -t: 
        done
}

function showAllShells(){

	llista_shells=$(cut -f7 -d: /etc/passwd | sort -u)
	for shell in $llista_shells
	do
		echo "$shell: "
		grep ":$shell$" /etc/passwd | cut -d: -f1,3,4,6 | sort -k2n -t:
	done

}


function showAllShells2(){

        llista_shells=$(cut -f7 -d: /etc/passwd | sort -u)
        for shell in $llista_shells
        do
		count=$(grep ":$shell$" /etc/passwd | wc -l)
                
		if [ $count -ge 2 ];
		then
			echo "$shell: $count"
                	grep ":$shell$" /etc/passwd | cut -d: -f1,3,4,6 | sort -k2n -t:
        	fi
	done

}


