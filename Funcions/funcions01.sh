#! /bin/bash
# Exemples de funcions
#

function creaEscola(){
	
	for classe in $*
	do
		creaClasse $classe
	done
}


function creaClasse(){
	classe=$1
	PASSWD="alum"
	llista_noms=$(echo ${classe}{01..02})
	for user in $llista_noms
	do
		useradd $user
		echo "$user:$PASSWD" | chpasswd
		#echo "alum" | passwd --stdin $user &> /dev/null
	done


}



function showAllGroups(){
	min_users=2
	llista_gids=$(cut -d: -f4 /etc/passwd | sort -n | uniq)
	for gid in $llista_gids
	do
		count=$(grep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd | wc -l)
		if [ $count -ge $min_users ];
		then
			gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)
			echo "$gname($gid): $count"
			grep "^[^:]*:[^:]*:[^:]*:$gid" /etc/passwd | sort | cut -d: -f1,3,7 | sed -r 's/^(.*)$/\t \1/'	
		fi
	done
	return 0

}


function suma(){
	echo "La suma és: $(($1+$2))"
	return 0
}

function dia(){
	date
	return 0
}

#validar rep 1 arg
#validar existeix usuari
#mostrar camp a camp
function showuser(){
	ERR_ARG=1
	ERR_USR=2
	
	if [ $# -ne 1 ];
	then
		echo "Error: numero d'arguments incorrecte"
		echo "Usage: $0 nom_usuari"
		return $ERR_ARG
	fi

	grep "^$1:" /etc/passwd
	if [ $? -ne 0 ];
	then
		echo "Error: el nom d'usuari no existeix"
		echo "Usage: $0 nom_usuari"
		return $ERR_USR
	fi
	login=grep "^$1:" /etc/passwd
	
	for arg in $login
	do
		echo $arg
	done
}

